/**
 ******************************************************************************
 * File Name          : main.c
 * Description        : Main program body
 ******************************************************************************
 *
 * COPYRIGHT(c) 2017 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */

/*****************************************************************************
 *****************************************************************************
 * Project Name		Messages from Harald	                                *
 * Author:          Erik Jonsson                                            *
 * Run with:     	TrueSTUDIO for ARM 7.0.1		                        *
 * Date:            2017-02-13						                        *
 * Description:     Display picture on oled screen.					 		*
 *****************************************************************************
 *****************************************************************************/

#include <string.h>
#include "font.h"

#define SCL(x)			HAL_GPIO_WritePin(SCL_GPIO_Port, SCL_Pin, x)
#define SDA(x)			HAL_GPIO_WritePin(SDA_GPIO_Port, SDA_Pin, x)
#define rSDA			HAL_GPIO_ReadPin(SDA_GPIO_Port, SDA_Pin)
#define LD2(x)			HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, x)	//LD2
#define HIGH			GPIO_PIN_SET
#define LOW				GPIO_PIN_RESET
#define CDELAY			cdelay()

#define ADDRESS			(0x3c<<1)
#define WRITE			0
#define READ			1
#define HORIZONTAL		0
#define VERTICAL		1
#define COMMAND			0x00
#define DATA			0x40
#define STARTBIT		SDA(LOW);  CDELAY	//SCK=HIGH
#define STOPBIT			SCL(HIGH); CDELAY; SDA(HIGH); CDELAY; CDELAY;	//SCK=HIGH

#define CONTRAST		0x81	//A
#define DISPLAYON(x)	(0xa4+x)
#define INVERSE(x)		(0xa6+x)
#define ONOFF(x)		(0xae +x)

#define LOCOL(x)		(x)
#define HICOL(x)		(0x10+x)
#define ADDRESSING		0x20	//A
#define SETCOL			0x21	//AB
#define SETPAGE			0x22	//AB
#define SETPAGESTART(x)	(0xb0+x)

#define STARTLINE(x)	(0x40+x)
#define REMAP(x)		(0xa0+x)
#define MULTIPLEX		0xa8	//A
#define OUTDIR(x)		(0xc0+(x<<3))
#define OFFSET			0xd3	//A
#define COMPINS			0xda	//A

#define DIVIDEFREQ		0xd5	//A
#define PRECHARGE		0xd9	//A
#define VCOMH			0xdb	//A
#define NOP				0xe3

#define CHARGEPUMP		0x8d	//A

#define DISABLESCROLL	0x2e

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART1_UART_Init(void);


/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

void cdelay(void);
void udelay(volatile unsigned int delay);
GPIO_PinState sendbyte(uint8_t data);
void printstr(char *string);
void printchr(uint8_t);
void clearScreen(void);


/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

	/* USER CODE BEGIN 1 */
	uint8_t input;							// Store char from UART
	char string[128] = {0};					// Store string from UART
	int i = 0;								// Foo var, (loop counters and such)
	char isInit = 0;						// Check if string has been sent once
	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	MX_USART1_UART_Init();

	/* USER CODE BEGIN 2 */

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */


	/* Initialize OLED display */
	STOPBIT;
	STARTBIT;
	LD2(sendbyte(ADDRESS+WRITE));
	LD2(sendbyte(COMMAND));
	LD2(sendbyte(ONOFF(0)));
	LD2(sendbyte(DIVIDEFREQ));	LD2(sendbyte(0x80));
	LD2(sendbyte(MULTIPLEX));	LD2(sendbyte(63));
	LD2(sendbyte(OFFSET));		LD2(sendbyte(0));
	LD2(sendbyte(STARTLINE(0)));
	LD2(sendbyte(CHARGEPUMP));	LD2(sendbyte(0x14));
	LD2(sendbyte(ADDRESSING));	LD2(sendbyte(HORIZONTAL));
	LD2(sendbyte(REMAP(1)));
	LD2(sendbyte(OUTDIR(1)));
	LD2(sendbyte(COMPINS));		LD2(sendbyte(0x12));
	LD2(sendbyte(CONTRAST));	LD2(sendbyte(0xcf));
	LD2(sendbyte(PRECHARGE));	LD2(sendbyte(0xf1));
	LD2(sendbyte(VCOMH));		LD2(sendbyte(0x40));
	LD2(sendbyte(DISPLAYON(0)));
	LD2(sendbyte(INVERSE(0)));
	LD2(sendbyte(DISABLESCROLL));
	LD2(sendbyte(ONOFF(1)));
	LD2(sendbyte(SETCOL));		LD2(sendbyte(0));	LD2(sendbyte(127));
	LD2(sendbyte(SETPAGE));		LD2(sendbyte(0));	LD2(sendbyte(7));
	STOPBIT;

	STARTBIT;
	LD2(sendbyte(ADDRESS+WRITE));
	LD2(sendbyte(DATA));

	HAL_UART_Transmit(&huart1,(uint8_t *)"Hello and welcome. To print a message on the OLED display: write a message, end with sndTxt and send, your message will be printed on the OLED. This can be repeated, older messages will be overwritten if a the 128 characters the screen can fit is exceeded. If you wish to clear the screen, simply send clrScn.\r\n",313,HAL_MAX_DELAY);

	for(int i = 0; i < 1024; i++)        // Clear screen
	{
		LD2(sendbyte(0x00));
	}
	strncpy(&string[0], "   Hello  and       welcome!    End message withsndTxt to print on screen. Send clrScn to clear screen. Enjoy!  ", 112);

	printstr(&string[0]);			// Print welcome message on OLED
	for(i = 0; i < 128; i++)		// Clear screen
		string[i] = 0;

	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

		HAL_UART_Receive(&huart1, (uint8_t *) &input, 1, HAL_MAX_DELAY);	// Receive character from UART
		strcat(&string[0], (char *)&input);		// Add character received from UART to string
		input = 0;

		if(strstr(string, "sndTxt"))		// If string contain sndTxt message will be printed
		{
			i = 0;

			while(string[i] != 0)
			{
				i++;
			}
			string[i - 6] = 0;				// chop off "sndTxt"
			if (isInit)
				printstr(&string[1]);		// Seems the first char in the message sent last is somehow stuck i a buffer or something.
											// After quite a bit of experimentation this was the only solution I found.
			else
			{
				clearScreen();				// Is this the first message sent, well let's delete that welcome message
				printstr(&string[0]);		// Nothing stuck in the buffer here
				isInit++;					// Next time skip index 0 as it will contain a scrap value from the previous message
			}
			for(; i > 0; i--)
				string[i] = 0;				// Empty string
		}

		if(strstr(string, "clrScn"))		// Check if clrScn has been sent, if so, let's clear the screen
		{
			clearScreen();					// Function call to clear the screen
			for(i = 0; i < 128; i++)
				string[i] = 0;				// Empty string
			isInit = 0;
			printstr(">");					// print a prompt on the OLED
		}


	}

	/* USER CODE END 3 */

}

/** System Clock Configuration
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;

	/**Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 100;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
	{
		Error_Handler();
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

	huart1.Instance = USART1;
	huart1.Init.BaudRate = 115200;
	huart1.Init.WordLength = UART_WORDLENGTH_8B;
	huart1.Init.StopBits = UART_STOPBITS_1;
	huart1.Init.Parity = UART_PARITY_NONE;
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart1) != HAL_OK)
	{
		Error_Handler();
	}

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		Error_Handler();
	}

}

/** Configure pins as 
 * Analog
 * Input
 * Output
 * EVENT_OUT
 * EXTI
 */
static void MX_GPIO_Init(void)
{

	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin : B1_Pin */
	GPIO_InitStruct.Pin = B1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : DIO_Pin */
	GPIO_InitStruct.Pin = DIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(DIO_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : CLK_Pin */
	GPIO_InitStruct.Pin = CLK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(CLK_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : LD2_Pin SCL_Pin */
	GPIO_InitStruct.Pin = LD2_Pin|SCL_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : STB_Pin */
	GPIO_InitStruct.Pin = STB_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(STB_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : SDA_Pin */
	GPIO_InitStruct.Pin = SDA_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(SDA_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC, DIO_Pin|CLK_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, LD2_Pin|SCL_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, STB_Pin|SDA_Pin, GPIO_PIN_RESET);

}

/* USER CODE BEGIN 4 */

void cdelay(void)
{
	volatile int delay;
	for (delay=8;delay;delay--);
}

void udelay(volatile unsigned int delay)
{
	delay*=11.206;
	for (;delay;delay--);
}


/* Empty screen and set address to top left */
void clearScreen(void)
{
	for(int i = 0; i < 128; i++)
		printchr(0);
	STOPBIT;
	STARTBIT;
	LD2(sendbyte(ADDRESS + WRITE));
	LD2(sendbyte(SETCOL));
	LD2(sendbyte(0));
	LD2(sendbyte(127));
	LD2(sendbyte(SETPAGE));
	LD2(sendbyte(0));
	LD2(sendbyte(7));
	STOPBIT;
	STARTBIT;
	LD2(sendbyte(ADDRESS + WRITE));
	LD2(sendbyte(DATA));
}

GPIO_PinState sendbyte(uint8_t data)
{
	GPIO_PinState ack;


	for(int i = 128; i != 0; i >>= 1)		// Loop through bits in byte, (0x80 >>= 1 == 0x40 and so on, i used as mask)
	{

		SCL(LOW);							// Clock set to low
		CDELAY;								// Wait so bit is sent while clock is low
		SDA(data & i);						// Set bit to be read, must be done while clock is low
		CDELAY;								// Wait	so bit is sent while clock is low
		SCL(HIGH);							// Clock set to high
		CDELAY;								// Let signal stabilize and the bit is read

	}
	SCL(LOW);								// Clock set to low
	CDELAY;									// Wait so next instruction is received properly
	SDA(HIGH);								// Set SDA to high to open drain, needed to read ack
	CDELAY;									// Wait while so SDA is changed while clock is low
	SCL(HIGH);								// Clock set to high
	CDELAY;									// Wait for signal to stabilize
	ack = rSDA;								// Read ack
	SCL(LOW);								// Set clock to low, ack is now read
	CDELAY;									// Wait for signal to stabilize

	return ack;
}

void printstr(char *string)
{
	int rowFilled = 0;						// Var to find empty spaces left to end of line

	while (*string != '\0')					// Find end of string character
	{
		printchr((uint8_t)*string++);		// Send each character in string individually to printchr which diplays it on the OLED screen
		rowFilled++;
	}

	while (rowFilled % 16)					// If rowFilled % 16 does not equal 0 fill out with spaces 'til it does
	{										// (16 characters fit on one line)
		printchr(' ');
		rowFilled++;
	}

}

void printchr(uint8_t uChar)
{
	for(int i = 0; i < 8; i++)				// Eight columns are needed to display one character using "font", hence each index in a character needs to be sent to display
	{
		LD2(sendbyte(font[(uChar * 8) + i]));	// Send column in character to OLED using sendbyte (256 characters in latin-1, 8 addresses are needed to display a character: character * 8 = index of first column in a character)
	}
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler */
	/* User can add his own implementation to report the HAL error return state */
	while(1)
	{
	}
	/* USER CODE END Error_Handler */
}

#ifdef USE_FULL_ASSERT

/**
 * @brief Reports the name of the source file and the source line number
 * where the assert_param error has occurred.
 * @param file: pointer to the source file name
 * @param line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */

}

#endif

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
